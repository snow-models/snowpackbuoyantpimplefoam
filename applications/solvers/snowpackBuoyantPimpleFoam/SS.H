Mg=Mair;
scalar initialDeltaTime=runTime.deltaTValue();
runTime.setDeltaT(steadyStateTimeStep, false);
Info<< "\nthe initial time step = " << initialDeltaTime << ", is changed to " << runTime.deltaTValue() << " for steady state solution.\n"  << endl;
//
for(int i=0; i<=numSteadyStateIter; i++)
{
	
	Info<< "\niteration =" << i << endl;
	#include "updateHeatAndMassTransferCoeffs.H"
	#include "UEqn.H"
	#include "EEqnSS.H"
	// --- Pressure corrector loop
	while (pimple.correct())
	{
		#include "pEqnSS.H"
	}
	Info<< "Maximum velocity    = " << mag(max(U).value()) << nl << endl;
	Info<< "the time step/ time    = " << runTime.deltaTValue() << ' ' << runTime.value() << endl;
}
//
runTime.setDeltaT(initialDeltaTime, false);
runTime.setTime(0, 0);
Info<< "\nthe initial time step is changed back to  " << runTime.deltaTValue() << " for transient solution.\n"  << endl;
Info<< "\nthe time step/time    = " << runTime.deltaTValue() << ' ' << runTime.value() << endl;
